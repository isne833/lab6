#include <iostream>
#include "fraction.h"
using namespace std;

fraction::fraction()
{
	num = 1;
	denom = 2;
}

fraction::fraction(int newnum)
{
	num = newnum;
	denom = 1;
}

fraction::fraction(int newnum, int newdenom)
{
	num = newnum;
	if (newdenom == 0)		//check if denominator = 0
	{
		cout << "ERROR" << endl;
		exit(0);
	}
	else
	{
		denom = newdenom;
	}
}

int fraction::getnum()
{
	return num;
}

int fraction::getdenom()
{
	return denom;
}

void fraction::setnum(int newnum)
{
	num = newnum;
}

void fraction::setdenom(int newdenom)
{
	if (newdenom == 0)		//check if denominator = 0
	{
		cout << "ERROR" << endl;
		exit(0);
	}
	else
	{
		denom = newdenom;
	}
	denom = newdenom;
} 

void fraction::showfrac()
{
	cout << num << "/" << denom << endl;
}

bool operator == (fraction f1, fraction f2)
{
	if (f1.num == f2.num && f1.denom == f2.denom)
	{
		return true;
	}
	else
	{
		return false;
	}
}

fraction operator * (fraction f1, fraction f2)
{
	int numerator, denominator;
	numerator = f1.num * f2.denom;
	denominator = f1.denom * f2.denom;
	return fraction(numerator, denominator);
}

fraction operator + (fraction f1, fraction f2)
{
	int numerator, denominator;
	numerator = (f1.num * f2.denom) + (f2.num * f1.denom);
	denominator = f1.denom * f2.num;
	return fraction(numerator, denominator);
}

fraction operator - (fraction f1, fraction f2) 
{
	int numerator, denominator;
	numerator = (f1.num * f2.denom) - (f2.num * f1.denom);
	denominator = f1.denom * f2.num;
	return fraction(numerator, denominator);
}

fraction operator / (fraction f1, fraction f2) 
{
	int numerator, denominator;
	numerator = f1.num * f2.denom;
	denominator = f2.num * f1.denom;
	return fraction(numerator, denominator);
}

fraction operator ++ (fraction f1, int) 
{
	int numerator;
	numerator = (f1.num + f1.denom);
	return fraction(numerator, f1.denom);
}

fraction operator -- (fraction f1, int) 
{
	int numerator;
	numerator = (f1.num - f1.denom);
	return fraction(numerator, f1.denom);
}

bool operator != (fraction f1, fraction f2) 
{
	if (f1.num * f2.denom != f2.num * f1.denom) 
	{
		return true;
	}
	else 
	{
		return false;
	}
}

bool operator >= (fraction f1, fraction f2) 
{
	if (f1.num * f2.denom >= f2.num * f1.denom) 
	{
		return true;
	}
	else 
	{
		return false;
	}
}

bool operator <= (fraction f1, fraction f2) 
{
	if (f1.num* f2.denom <= f2.num* f1.denom) 
	{
		return true;
	}
	else 
	{
		return false;
	}
}

bool operator > (fraction f1, fraction f2) 
{
	if (f1.num * f2.denom > f2.num * f1.denom) 
	{
		return true;
	}
	else 
	{
		return false;
	}
}

bool operator < (fraction f1, fraction f2) 
{
	if (f1.num * f2.denom < f2.num * f1.denom) 
	{
		return true;
	}
	else 
	{
		return false;
	}
}
