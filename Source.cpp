#include <iostream>
#include "fraction.h"
using namespace std;

int main()
{
	fraction f1;
	fraction f2(2,4);

	cout << endl << "First fraction : ";
	f1.showfrac();
	cout << endl << "Second fraction : ";
	f2.showfrac();

	//(*)
	fraction f3;		
	f3 = f1 * f2;
	cout << endl << "f1 * f2 = ";
	f3.showfrac();	

	//(+)
	fraction f4;		
	f4 = f1 + f2;
	cout << endl <<  "f1 + f2 = ";
	f4.showfrac();	

	//(-)
	fraction f5;	
	f5 = f1 - f2;
	cout << endl << "f1 - f2 = ";
	f5.showfrac();		

	//(/)			
	fraction f6;
	f6 = f1 / f2;
	cout << endl << "f1 / f2 = ";
	f6.showfrac();

	cout << endl << "First fraction (++) : ";
	f1++.showfrac();
	cout << endl << "Second fraction (++) : ";
	f2++.showfrac();
	cout << endl << "First fraction (--) : ";
	f1--.showfrac();
	cout << endl << "Second fraction (--) : ";
	f2--.showfrac();

	//Check ==, !=, >=, <=, >, <
	cout << endl << "If 1 = True, 0 = False.";
	cout << endl << "First fraction = Second fraction   : ";
	cout << (f1 == f2) << endl;

	cout << "First fraction != Second fraction  : ";
	cout << (f1 != f2) << endl;

	cout << "First fraction > Second fraction   : ";
	cout << (f1 > f2) << endl;

	cout << "First fraction < Second fraction   : ";
	cout << (f1 < f2) << endl;

	cout << "First fraction >= Second fraction  : ";
	cout << (f1 >= f2) << endl;

	cout << "First fraction <= Second fraction  : ";
	cout << (f1 <= f2) << endl;





	return 0;
}
