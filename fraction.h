#pragma once

class fraction
{
public:
	
	//constructor
	fraction();		
	fraction(int newnum);
	fraction(int newnum, int newdenom);


	//accessor
	int getnum();
	int getdenom();


	//mutator
	void setnum(int newnum);
	void setdenom(int denom);
	void showfrac();

	//overloading operator
	friend bool operator == (fraction f1, fraction f2);
	friend fraction operator * (fraction f1, fraction f2);
	friend fraction operator + (fraction f1, fraction f2);
	friend fraction operator - (fraction f1, fraction f2);
	friend fraction operator / (fraction f1, fraction f2);
	friend fraction operator ++ (fraction f1, int);
	friend fraction operator -- (fraction f1, int);
	friend bool operator != (fraction f1, fraction f2);
	friend bool operator >= (fraction f1, fraction f2);
	friend bool operator <= (fraction f1, fraction f2);
	friend bool operator > (fraction f1, fraction f2);
	friend bool operator < (fraction f1, fraction f2);



private:
	int num;
	int denom;



};
